import { LOGIN_SUCCESS, LOGIN_FAILURE } from '../constants/auth'

const initialState = {
    user: {},
    auth_data: {},
    auth_status: null
}

export default function auth(state = initialState, action) {

    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                auth_data: action.payload,
                auth_status: true
            }

        case LOGIN_FAILURE:
            return {
                ...state,
                auth_data: {},
                auth_status: null
            }

        default:
            return state;
    }

}