import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'
import axios from 'axios'

import Landing from '../components/landing'
import Header from '../components/common/header'
import Footer from '../components/common/footer'
import MainPage from '../components/mainPage'
import AddItem from '../components/addItem'
import Item from '../components/item'

class Inner extends Component {
	constructor(props){
		super(props)
	}
	
	

	render() {			
		return (
			<section>										
				<div>
				<Switch>
					<Route path="/inner/main" component={Landing}/>
					<Route path="/inner/head" component={Header}/>
					<Route path="/inner/footer" component={Footer}/>
					<Route exact path="/inner/additem" component={AddItem}/>
					<Route exact path="/inner/item/:id" component={Item}/>	
					<Route path="/inner/" component={MainPage}/>	
																		
				</Switch>
				</div>
			</section>
		)
	}
}

export default Inner