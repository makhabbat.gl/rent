import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'
import Modal from 'react-responsive-modal';
import NumericInput from 'react-numeric-input';
import { YMaps, Map, Circle } from 'react-yandex-maps';

import Header from '../common/header'
import Footer from '../common/footer'

class AddItem extends Component {
    constructor(props) {
        super(props)

    }
    state = {
        open: false,
      };
     
      onOpenModal = () => {
        this.setState({ open: true });
      };
     
      onCloseModal = () => {
        this.setState({ open: false });
      };
      
      geocode(ymaps) {
        console.log(ymaps)
    }
    render() {
        const { open } = this.state;
        const mapState = { center: [43.271034, 76.958502], zoom: 11 };
        // console.log(mapState)
        
        return ( 
            <section>
                <Header/>
                     <div className="container">
                        <div className="row">
                            <h1 className="add_item_headline">
                                Добавление объявлении
                            </h1>
                        </div>
                        <div className="row">
                            <div className="w-100 d-flex justify-content-center flex-column align-items-center ">
                                <div className="add_item_img text-center ">
                                    <img className="align-middle" src="#" className="d-none"  />
                                </div>
                                <div>
                                    <label className="link" htmlFor="download_photo" className="pointer"> Выберите фотографии </label>
                                    <input type="file" id="download_photo" className="d-none" multiple/>
                                </div>
                            </div>
                        </div>
                        <div className="row param_item">
                                <div className="param_item_title col-md-3 d-flex justify-content-center align-items-center">
                                    Название
                                </div>
                                <div className="col-md-6">
                                <input type="text" className="form-control form-control"/> 
                                </div>
                        </div>
                        <div className="row param_item">
                                <div className="param_item_title col-md-3 d-flex justify-content-center align-items-center">
                                    Рубрика
                                </div>
                                <div className="col-md-6">
                                <select class="form-control" id="exampleFormControlSelect2" onClick={this.onOpenModal}>
                                </select>
                                {/* <button >Open modal</button> */}
                                    
                                </div>
                        </div>
                        <Modal open={open} onClose={this.onCloseModal} center >
                            <div className="modal_wrapper d-flex justify-content-center align-items-center">
                                aaaaa
                            </div>
                        </Modal>
                        <div className="row param_item">
                                <div className="param_item_title col-md-3 d-flex justify-content-center align-items-center">
                                Срок аренды
                                </div>
                                <div className="col-md-6 d-flex justify-content-start align-items-center">
                                    <NumericInput min={0} max={100} value={50} className="form-control"className="form-control" />
                                    <div style={{'margin-left': '10px'}}>
                                    
                                        <select class="form-control" id="sel1">
                                            <option>День</option>
                                            <option>Неделя</option>
                                            <option>Месяц</option>
                                            <option>Год</option>
                                        </select>
                                        </div>
                                </div>
                        </div>
                        <div className="row param_item">
                                <div className="param_item_title col-md-3 d-flex justify-content-center align-items-center">
                                Цена
                                </div>
                                <div className="col-md-6">
                                <input type="text" className="form-control form-control"/> 
                            </div>
                        </div>
                        <div className="row param_item">
                                <div className="param_item_title col-md-3 d-flex justify-content-center align-items-start">
                                Описание
                                </div>
                                <div className="col-md-6">
                                <textarea class="form-control" rows="5" id="comment" name="text"></textarea>
                            </div>
                        </div>
                        <div className="row param_item">
                                <div className="param_item_title col-md-3 d-flex justify-content-center align-items-center">
                                Номер телефона
                                </div>
                                <div className="col-md-6">
                                <input type="text" className="form-control "/> 
                            </div>
                        </div>
                        <div className="row param_item">
                                <div className="param_item_title col-md-3 d-flex justify-content-center align-items-start">
                                Адрес
                                </div>
                                <div className="col-md-6">
                                <YMaps width={{'width':'100%'}} onApiAvaliable={ymaps => this.geocode(ymaps)}>
                                    <Map state={mapState} width={{'width':'100%'}}>
                                    <Circle
                                        geometry={{
                                      
                                            coordinates: [43.271034, 76.958502],
                                        
                                            radius: 2000,
                                        }}
                                        properties={{
                                            
                                            balloonContent: 'Радиус круга - 2 км',
                                    
                                            hintContent: 'Подвинь меня',
                                        }}
                                        options={{
                                            draggable: true,
                                        
                                            fillColor: '#DB709377',
                                        
                                            strokeColor: '#990066',
                                        
                                            strokeOpacity: 0.8,
                                        
                                            strokeWidth: 5
                                        }}
                                    />
                                    </Map>
                                </YMaps>
                            </div>

                        </div>
                        <div className="confirm_btn d-flex justify-content-center align-items-center">
                            <button type="button" class="btn btn-default">Default</button>
                        </div>
                        
                     </div>  
                     <Footer/> 
            </section> 
            
            // <style></style>
        )
    }
}


export default AddItem