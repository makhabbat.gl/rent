import React from 'react'
import { Link } from 'react-router-dom'


export default () => {
    return (
        <div className="w_100 h_100">
            <section>
                <div className="container">
                    <div className="row">
                        <div className="col text-center">
                            <div className="p-5">
                                <h1 className="kak">Как это работает?</h1>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex justify-content-center align-items-sm-center">
                        <img src="/img/left.jpg" alt="left" className="w_100px h_100px mt_90px mr_60px"/>
                        <p className="in_border border_3 tac cc">Регистрируйся</p>
                        <img src="/img/right.jpg" alt="left" className="w_100px h_100px mt_90px ml_60px"/>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 py-5">

                            <div className="d-flex flex-column justify-content-center align-items-sm-center">
                                <p className="in_border border_3 tac cc">Подай объявление</p>
                                <img src="/img/down.jpg" alt="icon" className="w_100px h_100pxx mt_60px"/>
                                <p className="in_border border_3 tac cc mt_40px ">Получай деньги</p>
                            </div>

                        </div>
                        <div className="col-sm-6 py-5">
                            <div className="d-flex flex-column justify-content-center align-items-sm-center">
                                <p className="in_border border_3 tac cc">Находи нужный товар</p>
                                <img src="/img/down.jpg" alt="icon" className="w_100px h_100px mt_60px "/>
                                <p className="in_border border_3 tac cc mt_40px ">Наслаждайся</p>
                            </div>
                        </div>
                    </div>
                    <hr className="hr"/>
                </div>
            </section>
        </div>
    )
}