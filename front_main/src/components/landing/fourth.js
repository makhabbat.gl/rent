import React from 'react'
import { Link } from 'react-router-dom'


export default () => {
    return (
        <div className="w_100 h_100">
            <section>
                <div className="container">
                <div className="row">
                        <div className="col text-center">
                            <div className="p-5">
                                <h1 className="kak">Наши партнеры</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-4 py-5">
                            <div className="d-flex justify-content-center align-items-sm-center">
                            <img src="/img/krysha.png" alt="icon" className="w_250px h_250px"/>
                            </div>
                        </div>
                        <div className="col-sm-4 py-5">
                            <div className="d-flex justify-content-center align-items-sm-center">
                            <img src="/img/kolesa.jpg" alt="icon" className="w_250px h_250px"/>
                            </div>
                        </div>
                        <div className="col-sm-4 py-5">
                            <div className="d-flex justify-content-center align-items-sm-center">
                            <img src="/img/market.png" alt="icon" className="w_250px h_250px"/>
                            </div>
                        </div>                        
                    </div>
                </div>
            </section>
        </div>
    )
}