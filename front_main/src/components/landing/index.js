import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'

import First from './first'
import Second from './second'
import Third from './third'
import App from './fourth'
import Footer from './footer'

class Landing extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return ( 
            <section>
            <First />
            <Second/>
            <Third/>
            <App/>
            <Footer/>
            </section>            
        )
    }
}

export default Landing