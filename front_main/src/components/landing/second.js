import React from 'react'
import { Link } from 'react-router-dom'


export default () => {
    return (
        <div className="w_100 h_100">
            <section>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 py-5">
                            <div className="col text-center">
                                <div className="p-5">
                                    <img src="/img/1.jpg" alt="icon" className="w_105px h_105px"/>
                                    <p className="mt_60px icon_text">Экономия времени и денег</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 py-5">
                            <div className="col text-center">
                                <div className="p-5">
                                    <img src="/img/2.jpg" alt="icon" className="w_105px h_105px"/>
                                    <p className="mt_60px icon_text">Сдай в аренду и зарабатывай деньги</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 py-5">
                            <div className="col text-center">
                                <div className="p-5">
                                    <img src="/img/3.jpg" alt="icon" className="w_105px h_105px"/>
                                    <p className="mt_60px icon_text">Находите нужные вещи в любом месте и в любое время</p>
                                </div>
                            </div>
                        </div>
                        <hr className="hr"/>
                    </div>
                </div>
            </section>
        </div>
    )
}