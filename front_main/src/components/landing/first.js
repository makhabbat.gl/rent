import React, {Component} from 'react'
import { Link } from 'react-router-dom'


class First extends Component{
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            active_state: 1
        }
    }

    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        })       
      }
    handleSubmit = event => {
        event.preventDefault();        
        const user = {
            email: this.state.email,
            password: this.state.password
        }
        signIn(user)       
    }

    handleSubmit2 = event => {
        event.preventDefault()
        const user = {
            name: this.state.name,
            surname: this.state.surname,
            email: this.state.email,
            password: this.state.password
        }
        // console.log(user)
        signUp(user)
    }

    setActive(form){
		this.setState({active_state: form})
    }
    
    render(){

        console.log(this.state.active_state)
    return (
        <div className="w_100 h_100">
            <section className="bg_img">
                <nav className="navbar navbar-expand-sm navbar-dark">
                    <div className="container pt_20px">
                    <a className="navbar-brand"><img src="/img/rental.png" alt="logo" className="w_200px h_90px"/></a>
                        <button className="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                        <div className="collapse navbar-collapse text-right" id="navbarCollapse">
                            <ul className="navbar-nav ml-auto">
                                <button className="color_white ff fs_22 border_login bg_none mr_20px cc w_120px vam h_55px tac nav-item" onClick={() => this.setActive(2)}>Вход</button>
                                <button className="color_white ff fs_22 bg_ffa01d border_n cc w_200px vam h_55px tac nav-item" onClick={() => this.setActive(4)}>Регистрация</button>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container">
                {
                    this.state.active_state == 1
                    ?
                    <div className="col-lg-4 float-right mr_40px ">
                        <div className="container">
                            <div className="row">
                                <div className=" text-right">
                                    <h1 className="color_white ff fs_45 w_400px fw_700 ls_1 lh_50px mt_90px h_110px">Экономь свое время и деньги </h1>
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="text-right">
                                    <p className="color_white ff fs_25 fw_400 w_400px h_110px">Удобная платформа для аренды всего необходимого</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    : this.state.active_state == 2
                    ?
                     <div className="col-lg-4 float-right mr-15px">                  
                            <div className="card text-center card-form">
                                <div className="card-body">
                                    <h3 className="vhod">Вход</h3> 
                                    <br/>                               
                                    <form>
                                        <div className="form-group">
                                            <input type="text" value={this.state.email} onChange={(e) => this.setState({...this.state, email: e.target.value})} className="card_input" placeholder="Email"/>
                                        </div>                                        
                                        <div className="form-group">
                                            <input type="password" value={this.state.password} onChange={(e) => this.setState({...this.state, password: e.target.value})} className="card_input" placeholder="Пароль"/>
                                        </div>
                                        <div className="tar mr_20px forget_pass" onClick={() => this.setActive(3)}>Забыли пароль?</div>
                                        <br/>
                                        <br/>                                        
                                        <button className="submit">Вход</button>
                                    </form>
                                </div>                     
                        </div>
                    </div>
                    : this.state.active_state == 3
                    
                    ?
                     <div className="col-lg-4 float-right mr-15px">                  
                        <div className="card text-center card-form">
                            <div className="card-body">
                                <h3 className="vhod">Забыли пароль?</h3> 
                                <br/>                               
                                <form>
                                    <div className="form-group">
                                        <input type="text" value={this.state.email} onChange={(e) => this.setState({...this.state, email: e.target.value})} className="card_input" placeholder="Email"/>
                                    </div>
                                    <br/>
                                    <br/>                                        
                                    <button className="submit">продолжить</button>
                                </form>
                            </div>                     
                        </div>
                    </div>
                    : this.state.active_state == 4 
                    ?
                    <div className="col-lg-4 float-right mr-15px">                  
                            <div className="card text-center card-form">
                                <div className="card-body">
                                    <h3 className="vhod">Регистрация</h3> 
                                    <br/>                               
                                    <form>

                                        <div className="form-group">
                                            <input type="text" value={this.state.name} onChange={(e) => this.setState({...this.state, name: e.target.value})} className="card_input" placeholder="Полное имя"/>
                                        </div>

                                        <div className="form-group">
                                            <input type="text" value={this.state.email} onChange={(e) => this.setState({...this.state, email: e.target.value})} className="card_input" placeholder="Email"/>
                                        </div>
                                        
                                        <div className="form-group">
                                            <input type="password" value={this.state.password} onChange={(e) => this.setState({...this.state, password: e.target.value})} className="card_input" placeholder="Пароль"/>
                                        </div>
                                        <br/>
                                        <br/>                                        
                                        <button className="submit">Вход</button>
                                    </form>
                                </div>                     
                        </div>
                    </div>
                    : null

                }           
    
                </div>
            </section>
        </div>
    )
}
}

export default First