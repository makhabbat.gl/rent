import React, {Component} from 'react'
import { Link } from 'react-router-dom'


class Categories extends Component{
    constructor(props) {
        super(props)
        this.state = {
            
        }
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="d-flex flex-row">
                            <div className="col-lg-8">
                                <span className="fa fa-search form-control-feedback"></span>
                                <input type="text" className="search mt_40px"/>
                            </div>
                            <div className="col-lg-4">
                                <input type="text" className="location mt_40px"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4 mt_50px">
                            <div className="d-flex flex-row justify-content-start align-items-sm-center">
                                <div className="icon_cat w_25px h_25px"><img src="/img/transport.png" className="w_45px h_40px mt_15px ml_13px" alt=""/></div>
                                <h1 className="category mt_20px ml_50px tac">Транспорт</h1>
                            </div>
                        </div>
                        <div className="col-md-4 mt_50px">
                            <div className="d-flex flex-row justify-content-start">
                                <div className="icon_cat w_25px h_25px"><img src="/img/electr.png" className="w_45px h_40px mt_15px ml_13px" alt=""/></div>
                                <h1 className="category mt_20px ml_50px tac">Электроника</h1>
                            </div>
                        </div>
                        <div className="col-md-4 mt_50px">
                            <div className="d-flex flex-row justify-content-start">
                                <div className="icon_cat w_25px h_25px"><img src="/img/home.png" className="w_45px h_40px mt_15px ml_13px" alt=""/></div>
                                <h1 className="category mt_20px ml_50px tac">Недвижимость</h1>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 mt_50px">
                            <div className="d-flex flex-row justify-content-start ">
                                <div className="icon_cat w_25px h_25px"><img src="/img/divan.png" className="w_45px h_40px mt_15px ml_13px" alt=""/></div>
                                <h1 className="category mt_20px ml_50px">Дом и сад</h1>
                            </div>
                        </div>
                        <div className="col-md-4 mt_50px">
                            <div className="d-flex flex-row justify-content-start">
                                <div className="icon_cat w_25px h_25px"><img src="/img/uslugi.png" className="w_45px h_40px mt_15px ml_13px" alt=""/></div>
                                <h1 className="category mt_20px ml_50px">Услуги      </h1>
                            </div>
                        </div>
                        <div className="col-md-4 mt_50px">
                            <div className="d-flex flex-row justify-content-start">
                                <div className="icon_cat w_25px h_25px"><img src="/img/moda.png" className="w_45px h_40px mt_15px ml_13px" alt=""/></div>
                                <h1 className="category mt_20px ml_50px">Мода и стиль</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="hr mt_80px"/>
            </div>
        )
    }
}

export default Categories