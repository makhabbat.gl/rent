import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'

import Header from '../common/header'
import Categories from './categories'

class MainPage extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return ( 
            <section>
                <Header/>
                <Categories/>            
            </section>            
        )
    }
}

export default MainPage