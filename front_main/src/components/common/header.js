import React from 'react'

export default () => {
	return (
		<div>
			<nav className="navbar navbar-expand-sm bg_2f6be4">
                <div className="container pt_20px">
                    <a className="navbar-brand"><img src="/img/rental.png" alt="logo" className="w_145px h_45px"/></a>
                    <button className="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                    <div className="collapse navbar-collapse" id="navbarCollapse">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <button className="btn btn-default btn-sm h_45px w_225px ttu cc color_white bg_ffa01d "><i className="fas fa-plus"></i> Подать объявление</button>
                            </li>
                            <li className="nav-item">
                            <div className="row ml_15px pt_10px">
                                <img src="/img/profile.png" className="w_25px h_25px"/>
                                <div className="btn h_45px mt-5px color_white ml-10px">Профиль</div>
                            </div>
                                {/* <div className="btn h_45px ml_10px color_white vac"><span><img src="/img/profile.png" className="w_25px h_25px mr_5px"/></span>Профиль</div> */}
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
		</div>
	)
}