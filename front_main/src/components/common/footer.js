import React from 'react'

export default () => {
	return (
		<div>
            <footer className="bg_2f6be4">
                <div className="container">
                    <div className="row">
                        <div className="col text-left">
                            <div className="py-4">
                            <div className="row">
                            <a className="navbar-brand"><img src="/img/rental.png" alt="logo" class="w_140px h_45px"/></a>
                            </div>
                                <div className="row">
                                    <img src="/img/phone.png" alt="icon" className="w_17px h_17px mr_15px"/>
                                    <p className="phone mt_3px">+7 775 932 4943</p>
                                </div>
                                <div className="row">
                                    <img src="/img/email.png" alt="icon" className="w_17px h_17px mt_15px mr_15px"/>
                                    <p className="phone mt_3px mt_15px">info@rental.com</p>
                                </div>
                                <div className="row">
                                    <p className="phone mt_20px">Almaty 2018 (c) Все права защищены</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
		</div>
	)
}